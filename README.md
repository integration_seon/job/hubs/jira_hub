# Hub

## Goal
The Hub is responsible to define if task will be processed by etl or realtime

## Requirements

| Program               | Version |
| --------------------- | ------: |
| Postgres              | 13.3    |
| MongoDB Community     | 4.4.5   |
| RabbitMQ              | 3.8.16  |
| Python                | 3.9.5   |
| Pip                   | 21.1.1  |

## Usage

Since it uses SRO_DB lib you must export the following environment variables:
* USERDB = Postgres's username
* PASSWORDDB = Postgres's password
* HOST = Postgres's host (usually localhost)
* DBNAME = Your database's name (must be already created) on Postgres

If your mongo db is not on your localhost you can export:

* MongoDB = Mongo db host address

If you would like to use a Cloud AMQP you must export:
* RABBITMQ_HOST = Url to connect (for more info https://www.cloudamqp.com/docs/index.html)


docker run -d --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management

Clone this repo and follow the steps bellow:

```bash
python3 -m venv env && source env/bin/activate
```

```bash
pip install -r requirements.txt
```

```bash
cd src/
./run.sh
```
