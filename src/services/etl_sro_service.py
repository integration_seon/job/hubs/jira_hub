import logging
logging.basicConfig(level=logging.ERROR)
from .sro_service import SROService
from jira_sro_etl import factories
from jira_sro_etl.manager import Manager
from datetime import datetime
import multiprocessing

def test(data):
    start_time = datetime.now()

    manager = Manager(data)
    manager.extract_all()
    manager.do_all()
    
    end_time = datetime.now()
    print('Elapsed Extract time: {}'.format(end_time - start_time))


class ETL_SRO_Service(SROService):

    def __init__(self):
        super().__init__()

    def do(self, journal):
        logging.info("ETL SRO Service: Start")
        try:
            super().do(journal)

            data = self.data
            p1 = multiprocessing.Process(target=test, args=(data,))
            p1.start()
            # p1.join() # If join process here, queue loses connection

            logging.info("ETL SRO Service: End")
        
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)  
            

