
from rabbitmqX.patterns.server.work_queue_task_server import Work_Queue_Task_Server
from service import Services
from dotenv import load_dotenv
import os
from skywalking import agent, config

load_dotenv()
try:
    
    config.init(collector_address=os.getenv('SW_AGENT_COLLECTOR_BACKEND_SERVICES', 'localhost:11800'), service_name='JIRA_HUB')

    agent.start()

    queue_name = "integration.jira"
    print ("Conectando na fila:{}".format(queue_name))

    services = Services()
    rpc_server = Work_Queue_Task_Server(queue_name,services,host=os.getenv('RABBITMQ_HOST', 'localhost'))
    rpc_server.start()

except Exception as e:
    print ("Error")
    print (e) 
