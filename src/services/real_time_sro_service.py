import logging
logging.basicConfig(level=logging.INFO)
from .sro_service import SROService
from jira_sro_etl import factories
from pprint import pprint

class Real_Time_SRO_Service(SROService):

    def __init__(self):
        
        super().__init__()

        self.jira_sro_etl_dict = {
            'user': factories.userFactory(),
            'project': factories.scrum_projectFactory(),
            'sprint': factories.sprintFactory(),
            'Story': factories.user_storyFactory(),
            'Epic': factories.epicFactory(),
            'Sub-task': factories.scrum_development_taskFactory(),
            'Subtask': factories.scrum_development_taskFactory()

        }
        
    def do(self, journal):
        
        logging.info("Real Time SRO Service: Start")
        
        super().do(journal)
        
        data = journal['data']
        
        pprint("---------Message From Queue------------")
        
        pprint(data)
        
        pprint("---------------------------------------")

        try:
            content = data['content']
            jira_sro_etl_class = self.jira_sro_etl_dict[content['type']]
            event = content['event_type']

            jira_sro_etl_class.config(self.data)
            if event == 'updated':
                jira_sro_etl_class.update(data)
            if event == 'created': 
                jira_sro_etl_class.create(data)
            if event == 'deleted': 
                jira_sro_etl_class.delete(data)

        except KeyError as e:
            logging.info(f"Non mapped entity <{content['type']}>")
            return

        except TypeError as e:
            logging.info(f"Empty content")
            return

        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)  


