from sro_db.application import factories
from requestx.RequestX import RequestX
import logging
logging.basicConfig(level=logging.INFO)

class SROService():

    def __init__(self):
        self.application_pp = factories.ApplicationFactory()
        self.organization_pp = factories.OrganizationFactory()
        self.configuration_pp = factories.ConfigurationFactory()
        self.data = None

    def __config(self, journal):
       
        data = journal["data"]
       
        organization_uuid = data['organization_uuid']
       
        configuration_uuid = data['configuration_uuid']

        organization = self.organization_pp.get_by_uuid(organization_uuid)
       
        configuration = self.configuration_pp.get_by_uuid(configuration_uuid)

        self.data = {
            'user': configuration.user,
            'key': configuration.secret,
            'url': configuration.url,
            'organization_id': organization_uuid,
            'configuration_id': configuration_uuid,
        }

    def do(self, journal):
        # This method must be overridden
        try:
            self.__config(journal)

            logging.info("SRO_Service: End")
        
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)  
         
            