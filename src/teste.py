
from rabbitmqX.patterns.server.work_queue_task_server import Work_Queue_Task_Server
from dotenv import load_dotenv
import os

from skywalking import agent, config

config.init(collector_address='172.19.140.96:11800', service_name='VaiMeufilho')

agent.start()

load_dotenv()

class Services():

    def do(self, string):
        print ("oi")


try:

    queue_name = "integration.jira"
    print ("Criando o Servidor")
    services = Services()

    rpc_server = Work_Queue_Task_Server(queue_name,services,host=os.getenv('RABBITMQ_HOST', 'localhost'))
    rpc_server.start()

except Exception as e:
    print (e) 
