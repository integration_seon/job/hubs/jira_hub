import importlib
import logging

logging.basicConfig(level=logging.INFO)

class Services(object):

    def do (self, journal):

        class_name = journal["type"]
        
        logging.info("Service: {} ".format(class_name))  

        service_object = getattr(importlib.import_module("services."+class_name.lower()+"_service"),
                                 class_name+"_Service")
        instance = service_object()
        return instance.do(journal)
